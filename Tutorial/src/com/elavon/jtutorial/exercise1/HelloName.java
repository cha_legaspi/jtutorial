package com.elavon.jtutorial.exercise1;

public class HelloName {

	public static void main(String[] args) throws java.lang.Exception {
		System.out.println(" **** *   *     *     *****   ***** *****");
		System.out.println("*     *   *    * *    *    *    *      *");
		System.out.println("*     *****   *****   *****     *     *");
		System.out.println("*     *   *  *     *  *   *     *    *");
		System.out.println(" **** *   * *       * *    *  ***** *****");
		System.out.println("");

		System.out.println("*       *     *     *         *     *****      *     *   *     *     *   *");
		System.out.println("**     **    * *    *        * *    *    *    * *    **  *    * *    **  *");
		System.out.println("* *   * *   *****   *       *****   *****    *****   * * *   *****   * * *");
		System.out.println("*  * *  *  *     *  *      *     *  *    *  *     *  *  **  *     *  *  **");
		System.out.println("*   *   * *       * ***** *       * *****  *       * *   * *       * *   *");
		System.out.println("");

		System.out.println("*     *****  *****      *      ****  *****  *****");
		System.out.println("*     *     *          * *    *      *    *   *");
		System.out.println("*     ***** *******   *****    ****  *****    *");
		System.out.println("*     *     *     *  *     *       * *        *");
		System.out.println("***** *****  *****  *       * *****  *      *****");
	}
}
